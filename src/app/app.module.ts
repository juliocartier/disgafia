import { LoginPageModule } from './../pages/login/login.module';
import { RegistroPageModule } from './../pages/registro/registro.module';
import { SairPageModule } from './../pages/sair/sair.module';
import { DicasPageModule } from './../pages/dicas/dicas.module';
import { DiarioPageModule } from './../pages/diario/diario.module';
import { AdicionaDiarioPageModule } from './../pages/adiciona-diario/adiciona-diario.module';
import { EditarDiarioPageModule } from './../pages/editar-diario/editar-diario.module';
import { TratamentoPageModule } from './../pages/tratamento/tratamento.module';
import { AnamnesePageModule } from './../pages/anamnese/anamnese.module';
import { VideosPageModule } from './../pages/videos/videos.module';

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SQLite } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { VideoDisfagiaPageModule } from '../pages/video-disfagia/video-disfagia.module';

/**
 * 
 * Todos os modulos do aplicativo
 * @export
 * @class AppModule
 */
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage
  ],
  imports: [
    BrowserModule,
    RegistroPageModule,
    LoginPageModule,
    SairPageModule,
    DicasPageModule,
    DiarioPageModule,
    TratamentoPageModule,
    AdicionaDiarioPageModule,
    EditarDiarioPageModule,
    AnamnesePageModule,
    VideosPageModule,
    VideoDisfagiaPageModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    Toast,
    YoutubeVideoPlayer,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
