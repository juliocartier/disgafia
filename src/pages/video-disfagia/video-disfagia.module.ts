import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoDisfagiaPage } from './video-disfagia';

@NgModule({
  declarations: [
    VideoDisfagiaPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoDisfagiaPage),
  ],
})
export class VideoDisfagiaPageModule {}
