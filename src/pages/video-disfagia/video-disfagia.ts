import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';


/**
 * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes
 * Generated class for the VideoDisfagiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-video-disfagia',
  templateUrl: 'video-disfagia.html',
})
export class VideoDisfagiaPage {

  video = "https://www.youtube.com/embed/FvFT0ccIVq0";

  constructor(public navCtrl: NavController, public navParams: NavParams, private dom: DomSanitizer) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideoDisfagiaPage');
  }

  sanitize2(video) {
    return this.dom.bypassSecurityTrustResourceUrl(video);
  }

}
