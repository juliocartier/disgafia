import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TratamentoPage } from '../tratamento/tratamento';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

/**
 * Generated class for the AnamnesePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-anamnese',
  templateUrl: 'anamnese.html',
})
export class AnamnesePage {

	dados : any;

	resposta_1_pag : any;
	resposta_2_pag : any;
	resposta_3_pag : any;
	resposta_4_pag : any;
	nivel_Disfagia : any;
	Qtd_Alimentos : any;
	Alimentos : any;
	Dieta_Geral : any;

	idRegistro: any = this.navParams.get('idRegistro');

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private sqlite: SQLite, private toast: Toast) {
  }

  ionViewDidLoad() {

    /**
     * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes 
     * é criado variaveis do tipo respostas para aparecer no menu principal as opções
     * para o usuário escolher, em cada pergunta se tem uma resposta.
    */
    this.resposta_1_pag = [{ resposta1: "Alta" }, { resposta1: "Leve" }, { resposta1: "Moderada" }, { resposta1: "Normal" }];

    this.resposta_2_pag = [{ resposta2: "Alta" }, { resposta2: "Leve" }, { resposta2: "Moderada" }, { resposta2: "Sem Retenção" }];

    this.resposta_3_pag = [{ resposta3: "Aspiracao Silenciosa" }, { resposta3: "Ausencia" }];

    this.resposta_4_pag = [{ resposta4: "Tosse Espontanea" }, { resposta4: "Tosse Fraca Voluntaria" }, { resposta4: "Tosse Voluntaria" }, { resposta4: "Voluntaria Ineficaz" }];
  }

  public saveData(resposta1 : any, resposta2 : any, resposta3 : any, resposta4 : any){

  	if (resposta1 == undefined && resposta2 == undefined && resposta3 == undefined && resposta4 == undefined) {
      /**
       * A varíavel alert, recebe os parametros da mensagem
       * para aparecer o alerta quando as variaveis são indefinidas
       */
      //console.log("Insira Dados");

      let alert = this.alertCtrl.create({
        title: 'Aviso',
        subTitle: 'Por favor, Insira os campos!',
        buttons: ['Ok']
      });

      alert.present();

    } else if (resposta1 == undefined || resposta2 == undefined || resposta4 == undefined){
    	 let alert = this.alertCtrl.create({
        title: 'Aviso',
        subTitle: 'Por favor, Insira os campos!',
        buttons: ['Ok']
      });

      alert.present();

      /** 
       * Caso o usuário, tenha uma resposta que seja equivalente as
       * condições é inserido no banco o nível de disfagia a quantidade
       * de alimentos, os alimentos e um tipo de dieta para o paciente.
      */
    }else if (resposta1 == 'Alta' && resposta2 == 'Alta' && resposta3 == 'Aspiracao Silenciosa' && resposta4 == 'Voluntaria Ineficaz') {
    	
    	console.log(resposta1, resposta2, resposta3, resposta4);
    	this.nivel_Disfagia = 'N1';
    	this.Qtd_Alimentos = '5';
    	this.Alimentos = '';
    	this.Dieta_Geral = '';

    	this.sqlite.create({
        name: 'teste.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS anamnese(id INTEGER PRIMARY KEY, nivel_disfagia TEXT, qtd_alimentos TEXT, alimentos TEXT, dieta_Geral TEXT, idRegistro INTEGER, FOREIGN KEY(idRegistro) REFERENCES registro(id))', {})
          .then(res => console.log('Executed SQL'))
          .catch(e => console.log(e));
        
          db.executeSql('INSERT INTO anamnese VALUES(NULL,?,?,?,?,?)', [this.nivel_Disfagia, this.Qtd_Alimentos, this.Alimentos, this.Dieta_Geral, this.idRegistro])
        .then(res => {
          console.log(res);
          this.toast.show('Anmanese', '5000', 'center').subscribe(
            toast => {
            	let dados  = { nivel_Disfagia : this.nivel_Disfagia, Qtd_Alimentos : this.Qtd_Alimentos, Alimentos : this.Alimentos, Dieta_Geral : this.Dieta_Geral, idRegistro : this.idRegistro };

               //console.log(dados); : this.Dieta_Geral, idRegistro : this.idRegistro

              this.navCtrl.push(TratamentoPage, dados);
            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });

  	
  	//let dados  = { nivelDisfagia : this.nivel_Disfagia, Qtd_Alimentos : this.Qtd_Alimentos, Alimentos: this.Alimentos, Dieta_Geral : this.Dieta_Geral};
  	
  	
  	}else if (resposta1 == 'Leve' && resposta2 == 'Leve' && resposta4 == 'Tosse Voluntaria') {
  		console.log(resposta1, resposta2, resposta4);
    	this.nivel_Disfagia = 'N2-N3';
    	this.Qtd_Alimentos = '5';
      this.Alimentos = 'Sopa Cremosa, Papa de Frutas, Purê de Batata, Creme de Ervilha, Mingau Grosso';
    	this.Dieta_Geral = 'Dieta Pastosa';

    	this.sqlite.create({
        name: 'teste.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS anamnese(id INTEGER PRIMARY KEY, nivel_disfagia TEXT, qtd_alimentos TEXT, alimentos TEXT, dieta_Geral TEXT, idRegistro INTEGER, FOREIGN KEY(idRegistro) REFERENCES registro(id))', {})
          .then(res => console.log('Executed SQL'))
          .catch(e => console.log(e));
        
          db.executeSql('INSERT INTO anamnese VALUES(NULL,?,?,?,?,?)', [this.nivel_Disfagia, this.Qtd_Alimentos, this.Alimentos, this.Dieta_Geral, this.idRegistro])
        .then(res => {
          console.log(res);
          this.toast.show('Anmanese', '5000', 'center').subscribe(
            toast => {
            	let dados  = { nivel_Disfagia : this.nivel_Disfagia, Qtd_Alimentos : this.Qtd_Alimentos, Alimentos : this.Alimentos, Dieta_Geral : this.Dieta_Geral, idRegistro : this.idRegistro };

               //console.log(dados); : this.Dieta_Geral, idRegistro : this.idRegistro

              this.navCtrl.push(TratamentoPage, dados);
            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });
  	
	  	//let dados  = { nivelDisfagia : this.nivel_Disfagia, Qtd_Alimentos : this.Qtd_Alimentos, Alimentos: this.Alimentos, Dieta_Geral : this.Dieta_Geral};
	  	//this.navCtrl.push(TratamentoPage, dados);

  	} else if (resposta1 == 'Moderada' && resposta2 == 'Moderada' && resposta4 == 'Tosse Fraca Voluntaria') {
  		// code...
  		console.log(resposta1, resposta2, resposta3, resposta4);
    	this.nivel_Disfagia = 'N4-N5';
    	this.Qtd_Alimentos = '5';
      this.Alimentos = 'Banana, Caldo de Feijão, Legumes Cozidos, Vitamina de Frutas, Sucos, Pão de Leite';
      this.Dieta_Geral = 'Dieta Light';

    	this.sqlite.create({
        name: 'teste.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS anamnese(id INTEGER PRIMARY KEY, nivel_disfagia TEXT, qtd_alimentos TEXT, alimentos TEXT, dieta_Geral TEXT, idRegistro INTEGER, FOREIGN KEY(idRegistro) REFERENCES registro(id))', {})
          .then(res => console.log('Executed SQL'))
          .catch(e => console.log(e));
        
          db.executeSql('INSERT INTO anamnese VALUES(NULL,?,?,?,?,?)', [this.nivel_Disfagia, this.Qtd_Alimentos, this.Alimentos, this.Dieta_Geral, this.idRegistro])
        .then(res => {
          console.log(res);
          this.toast.show('Anmanese', '5000', 'center').subscribe(
            toast => {
            	let dados  = { nivel_Disfagia : this.nivel_Disfagia, Qtd_Alimentos : this.Qtd_Alimentos, Alimentos : this.Alimentos, Dieta_Geral : this.Dieta_Geral, idRegistro : this.idRegistro };

               //console.log(dados); : this.Dieta_Geral, idRegistro : this.idRegistro

              this.navCtrl.push(TratamentoPage, dados);
            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });


  	
	  //	let dados  = { nivelDisfagia : this.nivel_Disfagia, Qtd_Alimentos : this.Qtd_Alimentos, Alimentos: this.Alimentos, Dieta_Geral : this.Dieta_Geral};
	 // 	this.navCtrl.push(TratamentoPage, dados);

  	} else if (resposta1 == 'Normal' && resposta2 == 'Sem Retenção' && resposta3 == 'Ausencia' && resposta4 == 'Tosse Espontanea') {
  		
  		console.log(resposta1, resposta2, resposta3, resposta4);
    	this.nivel_Disfagia = 'N6-N7';
    	this.Qtd_Alimentos = '5';
    	this.Alimentos = 'Pães, Grãos, Sucos, Carne, Leite, Vegetais';
    	this.Dieta_Geral = 'Dieta Geral'; 

    	this.sqlite.create({
        name: 'teste.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS anamnese(id INTEGER PRIMARY KEY, nivel_disfagia TEXT, qtd_alimentos TEXT, alimentos TEXT, dieta_Geral TEXT, idRegistro INTEGER, FOREIGN KEY(idRegistro) REFERENCES registro(id))', {})
          .then(res => console.log('Executed SQL'))
          .catch(e => console.log(e));
        
          db.executeSql('INSERT INTO anamnese VALUES(NULL,?,?,?,?,?)', [this.nivel_Disfagia, this.Qtd_Alimentos, this.Alimentos, this.Dieta_Geral, this.idRegistro])
        .then(res => {
          console.log(res);
          this.toast.show('Anmanese', '5000', 'center').subscribe(
            toast => {
            	let dados  = { nivel_Disfagia : this.nivel_Disfagia, Qtd_Alimentos : this.Qtd_Alimentos, Alimentos : this.Alimentos, Dieta_Geral : this.Dieta_Geral, idRegistro : this.idRegistro };

               //console.log(dados); : this.Dieta_Geral, idRegistro : this.idRegistro

              this.navCtrl.push(TratamentoPage, dados);
            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });

  	
	  	//let dados  = { nivelDisfagia : this.nivel_Disfagia, Qtd_Alimentos : this.Qtd_Alimentos, Alimentos: this.Alimentos, Dieta_Geral : this.Dieta_Geral};
	  	//this.navCtrl.push(TratamentoPage, dados);
  	
  	}else {

      let alert = this.alertCtrl.create({
        title: 'Aviso',
        subTitle: 'Não Existe um Tratamento!',
        buttons: ['Ok']
      });

      alert.present();

    }
  
  }

}
