import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';


/**
 * Generated class for the AdicionaDiarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adiciona-diario',
  templateUrl: 'adiciona-diario.html',
})
export class AdicionaDiarioPage {
  idRegistro: any = this.navParams.get('idRegistro');


  data = { data: "", tipoRefeicao: "", comida: "" };
  objeto_pag: any;

  /**
 * Creates an instance of AdicionaDiarioPage.
 * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes
 * @param {NavController} navCtrl 
 * @param {NavParams} navParams 
 * @param {SQLite} sqlite 
 * @param {Toast} toast 
 * @memberof AdicionaDiarioPage
 */

 constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite, private toast: Toast) {
    	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdicionaDiarioPage');

    this.objeto_pag = [{ tipo_Refeicao: "Café da Manhã" }, { tipo_Refeicao: "Almoço" }, { tipo_Refeicao: "Lanche da Tarde" }, { tipo_Refeicao: "Jantar" }];
  }

  /**
   * @memberof AdicionaDiarioPage
   * Esse método saveData tem como adicionar um diário no banco de dados
   * recebendo os valores como uma data, tipoRefeicao, comida e o idRegistro,
   * essa váriavel tem como funcionalidade receber o id de um login que foi cadastrado
   * e salvar no banco o diário só desse usário que fez o login
   * @tutorial socket-tutorial
   */
saveData() {
    this.sqlite.create({
      name: 'teste.db',
      location: 'default'
    }).then((db: SQLiteObject) => { //É inserido no banco de dados os dados na tabela diário junto com o id do usuário
      db.executeSql('INSERT INTO diario VALUES(NULL,?,?,?,?)', [this.data.data, this.data.tipoRefeicao, this.data.comida, this.idRegistro])
        .then(res => {
          console.log(res);
          this.toast.show('Salva', '5000', 'center').subscribe(
            toast => {
              this.navCtrl.popToRoot(); //Retorna para o menu principal
            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });
  }

}
