import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionaDiarioPage } from './adiciona-diario';

@NgModule({
  declarations: [
    AdicionaDiarioPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionaDiarioPage),
  ],
})
export class AdicionaDiarioPageModule {}
