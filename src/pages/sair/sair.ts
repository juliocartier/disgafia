import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes
 * Generated class for the SairPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sair',
  templateUrl: 'sair.html',
})
export class SairPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

      this.navCtrl.setRoot(LoginPage); //Retorna para o login

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SairPage');
  }

}
