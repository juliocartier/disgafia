import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

/**
 * Generated class for the TratamentoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tratamento',
  templateUrl: 'tratamento.html',
})
export class TratamentoPage {
	  expenses: any = [];
  	idRegistro: any = this.navParams.get('idRegistro');

  constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite, private toast: Toast) {
    /** 
     * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes 
     * É sempre apresentado só o nível de disfagia 
     * a partir do primeiro tratamento. É realizado
     * a consulta no banco de dados com os campos
     * id, nível da disfagiam qtd_Alimentos, alimentos
     * dieta getal e o id do usuário
    */
  	this.sqlite.create({
      name: 'teste.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('select id, nivel_disfagia, qtd_alimentos, alimentos, dieta_Geral, idRegistro from anamnese where idRegistro = ?', [this.idRegistro])
        .then(res => {
          this.expenses = [];
          //for (var i = 0; i < res.rows.length; i++) {
          for (var i = 0; i < 1; i++) {
            this.expenses.push({
              id: res.rows.item(i).id,
              nivel_disfagia: res.rows.item(i).nivel_disfagia,
              qtd_alimentos: res.rows.item(i).qtd_alimentos,
              alimentos: res.rows.item(i).alimentos,
              dieta_Geral: res.rows.item(i).dieta_Geral,
              idRegistro: res.rows.item(i).idRegistro
            })

          }
        }).catch(e => console.log(e));
      }).catch(e => console.log(e));


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TratamentoPage');
  }

}
