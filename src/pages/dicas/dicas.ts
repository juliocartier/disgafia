import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VideosPage } from './../videos/videos';
import { VideoDisfagiaPage } from './../video-disfagia/video-disfagia';


/**
 * Generated class for the DicasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dicas',
  templateUrl: 'dicas.html',
})
export class DicasPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  
/**
 * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes
 * A função videos é direcionada para os Videos que existe no aplicativo 
 * @memberof DicasPage
 */
public Videos(){
  	this.navCtrl.push(VideosPage); //Esse método navCtrl.push é levado para a página video
  }

  public VideoDisfagia(){
    this.navCtrl.push(VideoDisfagiaPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DicasPage');
  }

}
