import { RegistroPage } from './../registro/registro';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';



/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email : any;
  senha : any;
  nome : any;
  id : any;
  dados : any;
  

  myData = {id: "", nome: "", email : "", senha : ""};
/**
 * Creates an instance of LoginPage.
 * @param {NavController} navCtrl 
 * @param {NavParams} navParams 
 * @param {AlertController} alertCtrl 
 * @param {SQLite} sqlite 
 * @param {Toast} toast 
 * @memberof LoginPage
 */
constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private sqlite: SQLite, private toast: Toast) {
  }
  
/**
 * 
 * A função Registrar é direcionada para a tela de RegistroPage,
 * a partir dessa classe é possível criar um usuário para ter acesso
 * ao menu principal.
 * @memberof RegistroPage
 */
  public Registrar(){  
      this.navCtrl.setRoot(RegistroPage);
  }

  
/**
 *
 * @param {*} email 
 * @param {*} senha 
 * @memberof LoginPage
 * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes
 * A função Entrar recebe como um email e senha, caso esteja vazio
 * para se entrar é apresentado um alerta pedindo para inserir os campos
 * e caso o usuário não exista é apresentado um erro, assim a pessoa não consegue entrar
 * caso o email e senha esteja correto é feito uma consulta no banco de dados que recebe
 * como parametro esse email e senha, caso esteja é enviado para o menu principal com
 * os dados, como nome, id, email e senha.
 */
public Entrar(email : any, senha : any){

    if (email == undefined && senha == undefined) {

        /**
       * A varíavel alert, recebe os parametros da mensagem
       * para aparecer o alerta quando as variaveis são indefinidas
       */

      let alert = this.alertCtrl.create({
        title: 'Aviso',
        subTitle: 'Por favor, Insira os campos!',
        buttons: ['Ok']
      });

      alert.present();

    } else {
       this.sqlite.create({
       name: 'teste.db',
       location: 'default'
      }).then((db: SQLiteObject) => {
        /** 
         * Caso o usuário esteja inserido no banco e preenchido
         * corretamente, é feito uma consulta no banco e é retornado para o
         * menu principal
        */
        
        db.executeSql('select id, nome, email, senha from registro where email = ? and senha = ?', [email, senha])
          .then(res => {
        
              //this.myData.email = res.rows.item(0).email;
              //this.myData.senha = res.rows.item(0).senha;
              //this.nome =  res.rows.item(0).nome;

              //this.email = email;
              //this.senha = senha;

              //console.log(this.email, )

            //console.log(this.myData.email, this.myData.senha, this.nome);
              //this.email = this.myData.email;
              //this.senha = this.myData.senha;
              
              this.id = res.rows.item(0).id;
              this.email = res.rows.item(0).email;
              this.senha = res.rows.item(0).senha;
              this.nome =  res.rows.item(0).nome;   

              //Envia os dados do banco para o menu principal
             let dados  = { id : this.id, nome : this.nome, email : this.email, senha : this.senha };

               //console.log(dados);
            //É enviado para o menu principal os dados do banco.
              this.navCtrl.setRoot(HomePage, dados);
          })
          .catch(e => {
            console.log(e);
            this.toast.show(e, '5000', 'center').subscribe(
              toast => {
                console.log(toast);
              }
            );
          });
      }).catch(e => {
        console.log(e);
        this.toast.show(e, '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
      });
    }


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
