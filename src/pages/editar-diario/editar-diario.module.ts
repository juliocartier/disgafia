import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditarDiarioPage } from './editar-diario';

@NgModule({
  declarations: [
    EditarDiarioPage,
  ],
  imports: [
    IonicPageModule.forChild(EditarDiarioPage),
  ],
})
export class EditarDiarioPageModule {}
