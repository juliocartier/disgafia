import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

/**
 * Generated class for the EditarDiarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editar-diario',
  templateUrl: 'editar-diario.html',
})
export class EditarDiarioPage {

	myData = { id:0, data:"", tipoRefeicao:"", comida:"" };
	objeto_pag: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite, private toast: Toast) {
  	this.getCurrentData(navParams.get("id"));

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditarDiarioPage');

    this.objeto_pag = [{ tipo_Refeicao: "Breakfast" }, { tipo_Refeicao: "Lunch" }, { tipo_Refeicao: "Alternoon Snack" }, { tipo_Refeicao: "Dinner" }];
  }

  
/**
 * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes
 * @param {any} id 
 * @memberof EditarDiarioPage
 * O método de getCurrentData recebe o id e faz a busca do diário pelo id
 * a partir de buscar é apresentado os campos no formulário.
 */
getCurrentData(id) {
    this.sqlite.create({
      name: 'teste.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM diario WHERE id=?', [id])
        .then(res => {
          if(res.rows.length > 0) {
            this.myData.id = res.rows.item(0).id;
            this.myData.data = res.rows.item(0).data;
            this.myData.tipoRefeicao = res.rows.item(0).tipoRefeicao;
            this.myData.comida = res.rows.item(0).comida;
          }
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });
  }

  
/**
 * @memberof EditarDiarioPage
 * A função recebe os parametros do banco para adicionar e atualizar todos os campos
 * daquele id a partir da tabela e volta para o login.
 */
updateData() {
    this.sqlite.create({
      name: 'teste.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('UPDATE diario SET data=?,tipoRefeicao=?,comida=? WHERE id=?',[this.myData.data,this.myData.tipoRefeicao,this.myData.comida,this.myData.id])
        .then(res => {
          console.log(res);
          this.toast.show('Refeicao Atualizado', '5000', 'center').subscribe(
            toast => {
              this.navCtrl.popToRoot();
            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });
  } 

}
