import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the VideosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-videos',
  templateUrl: 'videos.html',
})
export class VideosPage {

 /**
  * As variaveis vid e video, recebe os links dos videos
  * para ser recarregado no aplicativo
  * 
  * @memberof VideosPage
  */
 vid = "https://www.youtube.com/embed/dDMQub9Geeo";

  

  /**
   * Creates an instance of VideosPage.
   * @param {NavController} navCtrl 
   * @param {NavParams} navParams 
   * @param {DomSanitizer} dom 
   * @memberof VideosPage
   * 
   * O construtor dessa função, recebe parametros para que possa voltar a tela principal
   * como também um pacote dom, aonde é recebido os links dos videos que possa ser
   * recarregado no aplicativo.
   */
  constructor(public navCtrl: NavController, public navParams: NavParams, private dom: DomSanitizer) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideosPage');
  }

  /**
   * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes
   * Recebe como parametro uma variavel, essa variavel é o link do video
   * para que se possa ser apresentado na tela do aplicativo
   * @param vid
   */
    sanitize(vid){
  	return this.dom.bypassSecurityTrustResourceUrl(vid);
  }



}
