import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';


/**
 * Generated class for the RegistroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  nome : any;
  email: any;
  senha: any;

  myData = {nome : "", email : "", senha : ""};

  /**
 * Criar as instancias para RegistroPage.
 * @param {NavController} navCtrl 
 * @param {NavParams} navParams 
 * @param {AlertController} alertCtrl 
 * @param {SQLite} sqlite 
 * @param {Toast} toast 
 * @memberof RegistroPage
 */

constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private sqlite: SQLite, private toast: Toast) {
  }

  /**
   * Essa função Voltar, ela tem como principal função voltar
   * a tela de login recebendo um navCtrl.setRoot, faz a rota
   * para a tela de Login
   * @memberof RegistroPage
   */

  public Voltar(){
    
    this.navCtrl.setRoot(LoginPage); //Esse método navCtrl.setRoot é levado para a página Login
    
  }

  
/**
 * 
 * @param myData.nome
 * @param myData.email
 * @param myData.senha
 * @memberof RegistroPage
 * 
 * A função cadastrar tem como a finalidade de salvar um usuário no banco de dados
 * para se ter acesso ao aplicativo. Recebe como parametro um email, senha e um nome
 * desses dados verifica se está vazio ou não, se estiver vazio apresenta um alerta
 * dizendo para preencher os campos. Caso não esteja vazio é criado o banco chamado
 * registro com os seguintes campos mencionados anteriormente, depois que enviado
 * os dados ao banco, é retornado a tela de login pelo metodo definido do framework
 * navCtrl.setRoot(LoginPage). 
 */
public Cadastrar(){

    if (this.myData.nome == undefined && this.myData.email == undefined && this.myData.senha == undefined){

      //console.log("Insira Dados");
      
      /**
       * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes
       * A varíavel alert, recebe os parametros da mensagem
       * para aparecer o alerta quando as variaveis são indefinidas
       */

      let alert = this.alertCtrl.create({
        title: 'Aviso',
        subTitle: 'Por favor, Insira os campos!',
        buttons: ['Ok']
      });

        alert.present();

    } else {

      this.sqlite.create({
        name: 'teste.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS registro(id INTEGER PRIMARY KEY, nome TEXT, email TEXT, senha INT)', {}) //É criado a tabela registro
          .then(res => console.log('Executed SQL'))
          .catch(e => console.log(e));
        
          db.executeSql('INSERT INTO registro VALUES(NULL,?,?,?)', [this.myData.nome, this.myData.email, this.myData.senha]) //É inserido no banco os dados do registro
        .then(res => {
          console.log(res); //Aparece a seguinte a mensagem caso o usuário seja inserido com sucesso.
          this.toast.show('Registrado', '5000', 'center').subscribe(
            toast => {
              this.navCtrl.setRoot(LoginPage); //Retorna a página Login
            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });

      

    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroPage');
  }

}
