import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AdicionaDiarioPage } from './../adiciona-diario/adiciona-diario';
import { EditarDiarioPage } from './../editar-diario/editar-diario';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/**
 * Generated class for the DiarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-diario',
  templateUrl: 'diario.html',
})
export class DiarioPage {

  expenses: any = [];
  dados : any;
  idRegistro: any = this.navParams.get('idRegistro');

  
/**
 * Creates an instance of DiarioPage.
 * @param {NavController} navCtrl 
 * @param {NavParams} navParams 
 * @param {SQLite} sqlite 
 * @memberof DiarioPage
 * A classe Diario, ela tem como adicionar um diário alimentar, editar
 * esse diário, deletar e visualizar (CRUD).
 * A partir do idRegistro é possível adicionar só aquele diário da 
 * pessoa logada pelo sistema.
 * 
 */
constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite) {
  

  }

  /**
   * @memberof DiarioPage
   * Recebe como paramétro o idRegistro para ir
   * a tela de adicionar um diário.
   */
  
  public AddDiario(){
  	
  	let dados  = { idRegistro : this.idRegistro};
  	//console.log(dados);

  	this.navCtrl.push(AdicionaDiarioPage, dados);
  }

  ionViewDidLoad() {
    this.getData();
  }

  ionViewWillEnter() {
    this.getData();
  }


  /**
   * @memberof DiarioPage
   * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes
   * O método getData cria uma tabela Diario caso não exista essa tabela,
   * a partir da criação dessa tabela é criado os campos de um id
   * uma data, tipoRefeição, comida e o idRegistro do usuário.
   * Caso tenha algum diário inserido é feito uma listagem de todo
   * o diario e apresentado na tela principal.
   */
  getData() {
    this.sqlite.create({
      name: 'teste.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('CREATE TABLE IF NOT EXISTS diario(id INTEGER PRIMARY KEY, data TEXT, tipoRefeicao TEXT, comida text, idRegistro INTEGER, FOREIGN KEY(idRegistro) REFERENCES registro(id))', {})
        .then(res => console.log('Executed SQL'))
        .catch(e => console.log(e));
      db.executeSql('select id, data, tipoRefeicao, comida, idRegistro from diario where idRegistro = ?', [this.idRegistro])
        .then(res => {
          this.expenses = []; //A variavel recebe um array e percorre todos os dados do banco para aparecer no diário
          for (var i = 0; i < res.rows.length; i++) {
            this.expenses.push({
              id: res.rows.item(i).id,
              data: res.rows.item(i).data,
              tipoRefeicao: res.rows.item(i).tipoRefeicao,
              comida: res.rows.item(i).comida,
              idRegistro: res.rows.item(i).idRegistro
            })

          }
        }).catch(e => console.log(e));
      }).catch(e => console.log(e));
}


/**
 * @param {any} id 
 * @memberof DiarioPage
 * Recebe como parametro um id e é enviado
 * para a tela Editar Diario, aonde é possível editar
 * os campos do diário
 */
editData(id) {
  this.navCtrl.push(EditarDiarioPage, {
    id:id
  });
}

/**
 * @param {any} id 
 * @memberof DiarioPage
 * Recebe um parametro id, a partir desse parametro
 * é feito a exclusão dos campos do id, incluido o idRegistro.
 */
deleteData(id) {
  this.sqlite.create({
    name: 'teste.db',
    location: 'default'
  }).then((db: SQLiteObject) => {
    db.executeSql('DELETE FROM diario WHERE id=?', [id])
    .then(res => {
      console.log(res);
      this.getData();
    })
    .catch(e => console.log(e));
  }).catch(e => console.log(e));
}

}
