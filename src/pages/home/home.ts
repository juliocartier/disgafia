import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DiarioPage } from './../diario/diario';
import { DicasPage } from './../dicas/dicas';
import { TratamentoPage } from './../tratamento/tratamento';
import { AnamnesePage } from './../anamnese/anamnese';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  nome: string = this.navParams.get('nome');
  idRegistro: any = this.navParams.get('id');
  dados : any;
  
/**
 * @author Cynthia Moreira Maia, Igor Melo Mendes e Julio C. M. Gomes
 * Creates an instance of HomePage.
 * @param {NavController} navCtrl 
 * @param {NavParams} navParams 
 * @memberof HomePage
 * Os dados enviados do login é recebido no menu principal,
 * com o seguinte método do framework, navParams.get,
 * é recebido para essa função o nome e o id do usuário realizado o login.
 */
constructor(public navCtrl: NavController, public navParams: NavParams) {

    /*
    variavel para receber o nome do usuário feito o login
    apresentando na tela principal.
    */  
    let nome = this.nome;
  
  }

  
/**
 * @memberof HomePage
 * A função tem como ir a tela do diário
 * para ser criado um diário do seu historico alimentar diário
 * o que foi realizado de habitos alimentares, é enviado o idRegistro
 * para que se possa adicionar o diário só daquele usuário.
 */
public Diario(){
  	let dados  = { idRegistro : this.idRegistro};
  	this.navCtrl.push(DiarioPage, dados);
  }
/**
 * @memberof HomePage
 * A função tem como ir para tela de Dicas
 */
public Dicas(){
  	 this.navCtrl.push(DicasPage);
  }

  
/**
 * @memberof HomePage
 * A função tratamento tem como ir a tela do tratamento
 * para visualizar o tratamento indicado a partir da anmanese feita pelo usuário
 * também é enviado o idRegistro para que se possa visualizar o tratamento 
 * só desse usuário.
 */
public Tratamento(){
  	let dados  = { idRegistro : this.idRegistro};

  	 this.navCtrl.push(TratamentoPage, dados);
  }

  /**
   * @memberof HomePage
   * A função Anamnese tem como ir a tela da Anamnese
   * para criar uma anamnese do usuário, a partir dessa anmanse é possível
   * visualizar o tratamento, é enviado o idRegistro para que se possa 
   * adicionar uma anmanse do usuário que foi feito o login.
   */
  public Anamnese(){
  	let dados  = { idRegistro : this.idRegistro};

  	 this.navCtrl.push(AnamnesePage, dados);
  }

}
