## Objetivo
O presente trabalho tem o propósito de auxiliar na recuperação 
dos pacientes com disfagia, fornecendo informações relevantes para a sua recuperação.

## Ambiente de Desenvolvimento
o framework Ionic é de código aberto, permitindo desenvolver aplicativos móveis híbridos usando tecnologias da web como: CSS, HTML, Apache Cordova e TypeScript. Seu processo é simplificado, garantindo um desenvolvimento rápido.

O NPM é um repositório online, de código aberto, utilizado para gerenciar os pacotes para desenvolvimento de aplicações.

## Instalacao no Windows
Para instalar, deve ser instalado primeiramente o NodeJS e o npm [NodeJS](https://nodejs.org/en/download/)

Depois de instalado o NodeJS e o NPM, basta executar o seguinte comando no terminal. 

npm install -g cordova ionic

## Baixando o Projeto
O projeto está hospedado no gitlab para ser baixado é necessário a instalação do git [GIT](https://git-scm.com/) Depois que instalado o git é necessário baixar o projeto.

git clone https://gitlab.com/juliocartier/disgafia.git

Depois de baixado é necessário executar no terminal na pasta do projeto aonde foi baixado o comando
npm install

e depois executar o ionic serve




